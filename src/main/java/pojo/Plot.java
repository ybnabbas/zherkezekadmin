package pojo;

public class Plot {
    int id;
    int queue;
    String iin;
    String fullName;
    String village;
    int id_village;
    String apply_date;
    int status;
    int size;
    String completion;


    public Plot() {
    }

    public Plot(int id, int queue, String iin, String fullName, String apply_date,String completion,String village,int id_village) {
        this.id = id;
        this.queue = queue;
        this.iin = iin;
        this.fullName = fullName;
        this.village = village;
        this.apply_date = apply_date;
        this.village=village;
        this.completion=completion;
        this.id_village=id_village;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public String getIin() {
        return iin;
    }

    public void setIin(String iin) {
        this.iin = iin;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getApply_date() {
        return apply_date;
    }

    public void setApply_date(String apply_date) {
        this.apply_date = apply_date;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCompletion() {
        return completion;
    }

    public void setCompletion(String completion) {
        this.completion = completion;
    }

    public int getId_village() {
        return id_village;
    }

    public void setId_village(int id_village) {
        this.id_village = id_village;
    }
}

