package pojo;

/**
 * Created by Ilyas on 15.10.2017.
 */
public class Question {
    int id;
    String imei;
    String iin;
    String question;
    String answer;
    String date;
    String phone;
    int status;

    public Question(String iin, String question, String answer, String date, String phone, int status) {
        this.iin = iin;
        this.question = question;
        this.answer = answer;
        this.date = date;
        this.phone = phone;
        this.status = status;
    }

    public Question() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getIin() {
        return iin;
    }

    public void setIin(String iin) {
        this.iin = iin;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
