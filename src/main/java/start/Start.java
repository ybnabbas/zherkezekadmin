package start;

import com.sun.javafx.application.LauncherImpl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.util.Locale;

public class Start extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Locale ru = new Locale.Builder().setLanguage("ru").build();
        Locale.setDefault(ru);
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/authorize.fxml"));
        primaryStage.setTitle("Жер кезек. Сайрам ауданы");

        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 270, 310));
        primaryStage.getIcons().add(new Image("/images/logo.png"));
        primaryStage.show();
    }


    public static void main(String[] args) {


        launch(args);
    }
}
