package utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class JSONCreater {
    URL url;
            HttpURLConnection connection;
    public JSONObject createObject(String address){
        StringBuilder sb = new StringBuilder();
        try {
            url= new URL(address);
            connection=(HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line=bufferedReader.readLine())!=null){
                sb.append(line+"\n");
            }
        }catch (IOException | JSONException e){
            e.printStackTrace();
            return null;
        }
        return new JSONObject(sb.toString());
    }
    public JSONArray createArray(String address){
        try {
            url =  new URL(address);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line=br.readLine())!=null){
                sb.append(line+"\n");
            }
            return new JSONArray(sb.toString());
        }catch (IOException|JSONException e){
            e.printStackTrace();
            return null;
        }
    }

}
