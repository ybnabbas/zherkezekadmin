package utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONService {

    JSONCreater jsonHelper = new JSONCreater();

    public JSONObject getUser(String login, String password) {
        String adress = Prop.getProperty("server")+"moderator/" +"authorize?"+ "login=" + login + "&password=" + password + "";
        return jsonHelper.createObject(adress);
    }



    public JSONArray getPlots(String address) {
        return jsonHelper.createArray(Prop.getProperty("server")+"plot/"+address);
    }
    public JSONArray getQuestions(int id) {
        return jsonHelper.createArray(Prop.getProperty("server")+"moderator/questions/"+id);
    }
}
