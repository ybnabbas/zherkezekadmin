package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class Prop {
    static Properties prop = new Properties();


    public static String getProperty(String name){
        InputStream is=null;
        try {
            is = Prop.class.getClassLoader().getResourceAsStream("prop/server.properties");
            prop.load(is);
            return prop.getProperty(name);
        }catch (IOException e){
            e.printStackTrace();
            return null;
        } finally {
            if (is!=null){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
