package controllers;

import com.jfoenix.controls.JFXSnackbar;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.paint.Color;
import org.json.JSONObject;
import utils.Prop;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * Created by Ilyas on 31.07.2017.
 */
public class InfoController {
    public ChoiceBox infoChoiceBox;
    public TextField infoTextField;
    public TextArea infoTextArea;
    public Label infoAuthor;
    public AnchorPane infoAnchorpane;


    public void initialize(){
        ObservableList infoType= FXCollections.observableArrayList("Жаңалық","Конкурс және аукцион");
        infoChoiceBox.setItems(infoType);
        infoAuthor.setText(Id.getVillageName());
    }

    public void declare(ActionEvent actionEvent) {
        JFXSnackbar snackbar = new JFXSnackbar(infoAnchorpane);
String title=infoTextField.getText();
int item=infoChoiceBox.getSelectionModel().getSelectedIndex();
String blog=(String)infoChoiceBox.getSelectionModel().getSelectedItem();
String content=infoTextArea.getText();
String resultSnackBar=null;
      if (title.isEmpty()){
          infoTextField.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, null, null)));
          resultSnackBar="Тақырыбын жазыңыз";
      }else if (content.isEmpty() || content.length()<50){
          infoTextArea.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, null, null)));
          resultSnackBar="Мәтін 50 символдан көп болуы керек";
      }
       else if (item==-1){
          resultSnackBar="Айдар тандаңыз";
      }else {
           Alert alert = new Alert(Alert.AlertType.INFORMATION);
           alert.setTitle(blog);
           alert.setHeaderText("Ақпаратты жариялау керек па?");
          Optional<ButtonType> result = alert.showAndWait();
          if (result.isPresent() && result.get()==ButtonType.OK){
              Client client = ClientBuilder.newClient();
              JSONObject object = new JSONObject();
              object.put("title",title);
              object.put("content",content);
              object.put("type",item);
              object.put("author",Id.getVillageName());
              Response response=null;
              try {
                   response=client.target(Prop.getProperty("server")+"moderator/addinfo").request().post(Entity.entity(object.toString(), MediaType.APPLICATION_JSON_TYPE));

              }catch (Exception e){
                  e.printStackTrace();
                  resultSnackBar="Сервермен байланыс жоқ. Кейінрек қайталаңыз";
              }
                     if (response==null){
                 resultSnackBar="Сервермен байланыс жоқ. Кейінрек қайталаңыз";
             } else if (response.getStatus()==200){
                 resultSnackBar="Ақпарат жарияланды";
                 infoTextField.clear();
                 infoTextArea.clear();
             }

          }




      }
        snackbar.show(resultSnackBar, 3000);
    }
}
