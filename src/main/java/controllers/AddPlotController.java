package controllers;

import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXTextField;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.paint.Color;
import org.json.JSONObject;
import pojo.Plot;
import utils.Prop;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


public class AddPlotController {

    public AnchorPane changeAnchor;
    public JFXTextField iinField;
    public JFXTextField fullnameField;
    public DatePicker applyDate;

DateTimeFormatter formatter =DateTimeFormatter.ofPattern("dd.MM.yyyy");


    public void addApplication(Event event) {
        String iin = iinField.getText();
        String fullname = fullnameField.getText();
        JFXSnackbar snackbar = new JFXSnackbar(changeAnchor);
        String resultString = null;
        LocalDate date=applyDate.getValue();


        if (iin.isEmpty()) {
            iinField.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, null, null)));
            resultString = "ЖСН толтырыңыз";
        } else if (fullname.isEmpty()) {
            fullnameField.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, null, null)));
            resultString = "Аты-жөнін  толтырыңыз";
        } else if (date==null){
            resultString = "Өтініш қабылданған күнді көрсетіңіз";
        }else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Ақпарат");
            alert.setHeaderText(fullname + " базаға еңгізу керек па?");

            Optional<ButtonType> result = alert.showAndWait();

            if ((result.isPresent() && result.get() == ButtonType.OK)) {

                Client client = ClientBuilder.newClient();
                JSONObject object = new JSONObject();
                object.put("fullname", fullname);
                object.put("iin", iin);
                object.put("apply_date", formatter.format(date));
                object.put("id_village", Id.getVillageId());
                object.put("confirm", false);
                Response response = null;

                try {
                    response = client.target(Prop.getProperty("server") + "moderator/adduser").request().post(Entity.entity(object.toString(), MediaType.APPLICATION_JSON_TYPE));


                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (response == null) {
                    resultString = "Сервермен байланыс жоқ.Кейінрек қайталаңыз";

                } else if (response.getStatus() == 200) {


                    Plot newPlot = new Plot();
                    newPlot.setFullName(fullname);
                    newPlot.setIin(iin);
                    iinField.clear();
                    fullnameField.clear();
                    resultString = fullname + " Базаға қосылды";
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/list.fxml"));
                        Parent parent = loader.load();
                        ListController dac = (ListController) loader.getController();
                        dac.addPlot(newPlot);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (response.getStatus() == 409) {
                    Alert alert1 = new Alert(Alert.AlertType.ERROR);
                    alert1.setTitle("Қосымша ақпарат");
                    alert1.setHeaderText(fullname + " Базада бар. Тағыда қосу керек па?");
                    Optional<ButtonType> result1 = alert1.showAndWait();
                    if ((result1.isPresent() && result1.get() == ButtonType.OK)) {
                        object.put("confirm", true);
                        try {
                        response = client.target(Prop.getProperty("server") + "moderator/adduser").request().post(Entity.entity(object.toString(), MediaType.APPLICATION_JSON_TYPE));
                            System.out.println(object.toString());
                            if (response.getStatus() == 200) {
                                resultString = fullname + " базаға қосылды";
                            } else {
                                resultString = fullname + "  қосылмады ";
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            resultString = "Сервермен байланыс жоқ.Кейінрек қайталаңыз";
                        }


                    } else {
                        resultString = fullname + "  қосылмады ";
                    }

                } else {
                    System.out.println("Status : " + response.getStatus());
                    resultString = "Қосылмады. қайталаңыз";
                }
            } else {
                resultString = "Қосылмады";
            }
        }
        snackbar.show(resultString, 3000);
    }


}
