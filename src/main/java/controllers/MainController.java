package controllers;


import com.jfoenix.controls.JFXTabPane;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.awt.*;

public class MainController {

@FXML
    JFXTabPane mainTabPane;
    public Tab formalize;
    public Tab list;
    public Tab info;
    public Tab setting;
    public Tab question;

    public void initialize(){
        if (Id.getGroup()==1){

            mainTabPane.getSelectionModel().select(list);
            mainTabPane.getTabs().remove(0);

        }else {
            mainTabPane.getSelectionModel().select(formalize);
        }




    }




}
