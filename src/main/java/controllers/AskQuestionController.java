package controllers;

import com.jfoenix.controls.JFXSnackbar;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.json.JSONObject;
import pojo.Question;
import utils.Prop;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


public class AskQuestionController {
    public Label questionId;
    public Label questionLabel;
    public Label questionDate;
    public Label questioniin;
    public Label questionPhone;
    public TextArea answerEditor;
    public AnchorPane anchorpane;
    Question question;


    public void setQuestion(Question question) {
        this.question = question;

    }

    public void init() {
        questionId.setText(question.getId() + "");
        questionLabel.setText(question.getQuestion());
        questionDate.setText(question.getDate());
        questioniin.setText(question.getIin());
        questionPhone.setText(question.getPhone());
        answerEditor.setText(question.getAnswer());

    }

    public void send(ActionEvent actionEvent) throws InterruptedException {
        JFXSnackbar snackbar = new JFXSnackbar(anchorpane);
        String answer = answerEditor.getText();

        if (answer.length() < 15) {
            snackbar.show("Жауап толық емес", 2500);
        } else {
            JSONObject object = new JSONObject();
            object.put("id",question.getId());
            object.put("answer",answerEditor.getText());
            object.put("imei", question.getImei());
            if (sendAnswer(object).getStatus() == 200) {
                Dialog alert = new Alert(Alert.AlertType.INFORMATION, "Жауап жөнетілді", ButtonType.OK);
                alert.setTitle("Ақпарат");
                alert.setHeaderText(null);
                alert.show();
                alert.setOnCloseRequest(event -> {
                    ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
                });
            } else {
                snackbar.show("Сервермен байланыс жоқ.Қайталаңыз", 2500);
            }


        }
    }

    private Response sendAnswer(JSONObject object) {
        System.out.println(object.toString());
        Response response = null;
        Client client = ClientBuilder.newClient();
        try {
            return response = client.target(Prop.getProperty("server") + "moderator/answer").request().post(Entity.entity(object.toString(), MediaType.APPLICATION_JSON_TYPE));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

    public void cancel(ActionEvent actionEvent) {
        ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
    }
}
