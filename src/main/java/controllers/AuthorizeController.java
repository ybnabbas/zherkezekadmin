package controllers;

import com.jfoenix.controls.JFXCheckBox;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.json.JSONObject;
import utils.JSONService;

import java.io.IOException;
import java.util.concurrent.*;

public class AuthorizeController {

    public JFXCheckBox pass_toggle;
    JSONService jsonService = new JSONService();

    @FXML
    TextField logintxtField;
    @FXML
    PasswordField passwordtxtField;
    @FXML
    Label result;
    @FXML
    AnchorPane anchorpane;
    public ProgressIndicator progressindicator;


    public void getLogin() throws ExecutionException, InterruptedException {
        showIndicator(true);
        ExecutorService executor = Executors.newFixedThreadPool(2);
        Callable callable = () -> {
            try {
                JSONObject user = jsonService.getUser(logintxtField.getText(), passwordtxtField.getText());
                if (user.getBoolean("exist")) {
                    Id.exist = user.getBoolean("exist");
                    Id.id = user.getInt("id");
                    Id.village = user.getString("village");
                    Id.group = user.getInt("group");
                    Id.login = user.getString("login");
                    return true;
                } else return false;

            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

        };

        Future future = executor.submit(callable);
        Boolean resultFromTask = (Boolean) future.get();
        System.out.println("resultFromTask : " + resultFromTask);

        try {
            if (resultFromTask) {
                showIndicator(true);
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
                Stage stage = new Stage();

                stage.setResizable(false);
                System.out.println("resultFromTask : " + resultFromTask);
                Parent parent = (Parent) loader.load();
                Scene scene = new Scene(parent, 1000, 600);
                stage.setScene(scene);
                result.getScene().getWindow().hide();
                stage.show();
stage.setOnCloseRequest(event -> {
    System.exit(0);
});

            } else {
                result.setTextFill(Color.web("#EE3A23"));
                result.setText("Invalid user");
            }
        } catch (Exception e) {
            e.printStackTrace();
            showIndicator(false);
            result.setTextFill(Color.web("#EE3A23"));
            result.setText("Сервермен байланыс жоқ");

        }


    }

    private void showIndicator(boolean ok) {


        progressindicator.setVisible(ok);


    }


    public void showPassword(ActionEvent actionEvent) {
        if (pass_toggle.isSelected()) {
            passwordtxtField.setPromptText(passwordtxtField.getText());

        } else {
            passwordtxtField.setPromptText("Құпия сөз");
        }
    }
}
