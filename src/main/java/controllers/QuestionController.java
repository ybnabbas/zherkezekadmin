package controllers;


import com.jfoenix.controls.JFXSnackbar;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;
import pojo.Plot;
import pojo.Question;
import utils.JSONService;

import java.io.IOException;

public class QuestionController {

    public TableView tablequestion;
    JSONService jsonService = new JSONService();
    public AnchorPane questionAnchorPane;
    JFXSnackbar snackbar = new JFXSnackbar(questionAnchorPane);
   ObservableList<Question> questions = getQuestions(Id.getVillageId());

    public void initialize() {
        setValueTableColumn();
        tablequestion.setItems(questions);
        setClick();
    }


    private ObservableList<Question> getQuestions(int id) {
        ObservableList<Question> returner = FXCollections.observableArrayList();
        JSONArray jsonArray = jsonService.getQuestions(id);
        if (jsonArray == null) {
            snackbar.show("Бос", 3000);
        }
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = (JSONObject) jsonArray.get(i);
                Question question = new Question();
                question.setId(object.getInt("id"));
                question.setAnswer(object.getString("answer"));
                question.setStatus(object.getInt("status"));
                question.setDate(object.getString("date"));
                question.setQuestion(object.getString("question"));
                question.setIin(object.getString("iin"));
                question.setImei(object.getString("imei"));
                question.setPhone(object.getString("phone"));
              returner.add(question);
            }
        } catch (NullPointerException e) {

            JFXSnackbar snackbar = new JFXSnackbar(questionAnchorPane);
            snackbar.show("Сервермен байланыс жоқ", 3000);
        }
        return returner;
    }

    private void setValueTableColumn() {
        TableColumn<Question, Integer> colId = new TableColumn<>("id");
        TableColumn<Question, String> colIin = new TableColumn<>("ЖСН");
        TableColumn<Question, String> colQuestion = new TableColumn<>("Сұрақ");
        TableColumn<Question, String> colPhone = new TableColumn<>("Телефон");
        TableColumn<Question,String> colImei = new TableColumn<>("IMEI");
        TableColumn<Question, String> colDate = new TableColumn<>("Күні");
        TableColumn<Question, String> colAnswer = new TableColumn<>("Жауап");
        TableColumn<Question, Integer> colStatus = new TableColumn<>("Статус");

      colId.setCellValueFactory(new PropertyValueFactory<Question, Integer>("id"));
        colIin.setCellValueFactory(new PropertyValueFactory<Question, String>("iin"));
        colQuestion.setCellValueFactory(new PropertyValueFactory<Question, String>("question"));
        colPhone.setCellValueFactory(new PropertyValueFactory<Question, String>("phone"));
        colImei.setCellValueFactory(new PropertyValueFactory<Question, String>("imei"));
        colDate.setCellValueFactory(new PropertyValueFactory<Question, String>("date"));
        colAnswer.setCellValueFactory(new PropertyValueFactory<Question, String>("answer"));
        colStatus.setCellValueFactory(new PropertyValueFactory<Question, Integer>("status"));
        tablequestion.getColumns().addAll(colId,colIin,colQuestion,colPhone,colImei,colAnswer, colDate,colStatus);

    }

    private void setClick(){
        tablequestion.setRowFactory(tv->{
            TableRow tableRow = new TableRow();

            tableRow.setOnMouseClicked(event -> {
                if (event.getClickCount()==2){
                    Question selected=(Question) tableRow.getItem();
                    modalWindow(selected);
                }
            });
            return tableRow;
        });
    }

    private void modalWindow(Question question){
        try {

            FXMLLoader loader= new FXMLLoader(QuestionController.class.getClass().getResource("/fxml/ask_question.fxml"));
            Parent root= loader.load();
            AskQuestionController askQuestionController=loader.<AskQuestionController>getController();
            askQuestionController.setQuestion(question);
            askQuestionController.init();
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Жауап беру");
            stage.setResizable(false);
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(tablequestion.getScene().getWindow());

            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void refresh(ActionEvent actionEvent) {
        ObservableList<Question> refresh = getQuestions(Id.getVillageId());
        tablequestion.setItems(refresh);
    }
}
