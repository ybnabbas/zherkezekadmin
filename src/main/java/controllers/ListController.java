package controllers;


import com.jfoenix.controls.JFXSnackbar;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import listeners.FilterListener;
import listeners.MenuItemListener;
import org.json.JSONArray;
import org.json.JSONObject;
import pojo.Plot;
import utils.JSONService;

import java.util.List;

public class ListController {
    public AnchorPane listAnchorPane;

    public TabPane listTabPane;
    public Label labelCountApplication;
    public Label labelCountQueue;
    public Label labelCountGet;
    public Label labelCountRefuze;
    public Label labelCountRemove;
    public Label labelCountTempRemove;
    public Label labelOkrug;
    public Label labelLogin;
    public ComboBox comboBoxApplication;
    public ComboBox comboBoxQueue;
    public ComboBox comboBoxGet;
    public ComboBox comboBoxRefuze;
    public ComboBox comboBoxRemove;
    public ComboBox comboBoxTempRemove;
    public Label labelStatus;
    public TableView tableApplication;
    public TableView tableQueue;
    public TableView tableGet;
    public TableView tableRefuze;
    public TableView tableTempRemove;
    public TableView tableRemove;
    public TextField searchFieldApplication;
    public TextField searchFieldQueue;
    public TextField searchFieldGet;
    public TextField searchFieldRefuze;
    public TextField searchFieldRemove;
    public TextField searchFieldTempRemove;

    JSONService jsonService = new JSONService();
    JFXSnackbar snackbar = new JFXSnackbar(listAnchorPane);
    ObservableList<Plot> listApplication = getObservableList(Id.getVillageId(), 1);
    ObservableList<Plot> listQueue = getObservableList(Id.getVillageId(), 2);
    ObservableList<Plot> listGet = getObservableList(Id.getVillageId(), 3);
    ObservableList<Plot> listRefuze = getObservableList(Id.getVillageId(), 4);
    ObservableList<Plot> listRemove = getObservableList(Id.getVillageId(), 5);
    ObservableList<Plot> listTempRemove = getObservableList(Id.getVillageId(), 6);

    String[] villageList = {"", "Ақсукент ауылдық округ", "Ақбұлақ ауылдық округ", "Арыс ауылдық округ", "Қайнарбұлақ ауылдық округ",
            "Қарабұлақ ауылдық округ", "Қарамұрт ауылдық округ", "Қарасу ауылдық округ", "Көлкент ауылдық округ",
            "Құтарыс ауылдық округ", "Манкент ауылдық округ", "Жібек жолы ауылдық округ "};

    public void initialize() {
        labelOkrug.setText(Id.getVillageName());
        labelLogin.setText(Id.getlogin());
        tableApplication.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        switch (Id.getGroup()) {
            case 2:
                labelStatus.setText("Модератор");
                showComboBoxes(false);

                break;
            case 1:
                labelStatus.setText("Super Администратор");
                showComboBoxes(true);
                break;

        }


        tableQueue.setItems(listQueue);
        tableApplication.setItems(listApplication);
        tableGet.setItems(listGet);
        tableRefuze.setItems(listRefuze);
        tableRemove.setItems(listRemove);
        tableTempRemove.setItems(listTempRemove);

        labelCountApplication.setText(tableApplication.getItems().size() + "");
        labelCountQueue.setText(tableQueue.getItems().size() + "");
        labelCountGet.setText(tableGet.getItems().size() + "");
        labelCountRefuze.setText(tableRefuze.getItems().size() + "");
        labelCountRemove.setText(tableRemove.getItems().size() + "");
        labelCountTempRemove.setText(tableTempRemove.getItems().size() + "");

        setValueFactory(tableQueue);
        setValueFactory(tableApplication);
        setValueFactory(tableGet);
        setValueFactory(tableRefuze);
        setValueFactory(tableRemove);
        setValueFactory(tableTempRemove);

        filter(listApplication, searchFieldApplication, tableApplication, labelCountApplication, comboBoxApplication);
        filter(listQueue, searchFieldQueue, tableQueue, labelCountQueue, comboBoxQueue);
        filter(listGet, searchFieldGet, tableGet, labelCountGet, comboBoxGet);
        filter(listRefuze, searchFieldRefuze, tableRefuze, labelCountRefuze, comboBoxRefuze);
        filter(listRemove ,searchFieldRemove, tableRemove , labelCountRemove, comboBoxRemove);
        filter( listTempRemove,searchFieldTempRemove,  tableTempRemove,labelCountTempRemove, comboBoxTempRemove);

        contextMenu(tableApplication, Id.getGroup());
        contextMenu(tableQueue, Id.getGroup());
        contextMenu(tableRefuze, Id.getGroup());
        contextMenu(tableTempRemove, Id.getGroup());

    }
    private void filter(ObservableList list, TextField searchField, TableView table, Label result, ComboBox comboBox) {
        FilterListener listener = new FilterListener(list, table, result);
        comboBox.valueProperty().addListener(listener);
        searchField.textProperty().addListener(listener);

    }



    private void showComboBoxes(boolean show) {
        comboBoxApplication.getItems().addAll(villageList);
        comboBoxQueue.getItems().addAll(villageList);
        comboBoxGet.getItems().addAll(villageList);
        comboBoxRefuze.getItems().addAll(villageList);
        comboBoxRemove.getItems().addAll(villageList);
        comboBoxTempRemove.getItems().addAll(villageList);

        comboBoxApplication.setVisible(show);
        comboBoxQueue.setVisible(show);
        comboBoxGet.setVisible(show);
        comboBoxRefuze.setVisible(show);
        comboBoxRemove.setVisible(show);
        comboBoxTempRemove.setVisible(show);

    }

    private void contextMenu(TableView table, int id) {
        if (table.getItems().size() != 0) {

            //меню для таблицы  заявки
            ContextMenu menuApplication = new ContextMenu();
            MenuItem itemQueue = new MenuItem("Кезекке қосу");
            itemQueue.setId("toQueueItem");
            setEventItem(itemQueue, tableApplication, tableQueue);
            //
            MenuItem itemRefuze = new MenuItem("Қабылдамау");
            itemRefuze.setId("toRefuzeItem");
            setEventItem(itemRefuze, tableApplication, tableRefuze);
            menuApplication.getItems().addAll(itemQueue, itemRefuze);
            //

            //Меню для таблицы очереди
            ContextMenu menuQueue = new ContextMenu();
            MenuItem itemGet = new MenuItem("Жер беру");
            itemGet.setId("toGetItem");
            setEventItem(itemGet, tableQueue, tableGet);

            MenuItem itemRemove = new MenuItem("Кезектен шығару");
            itemRemove.setId("toRemoveItem");
            setEventItem(itemRemove, tableQueue, tableRemove);

            MenuItem itemTempRemove = new MenuItem("Кезектен уақытша шығару");
            itemTempRemove.setId("toTempRemoveItem");
            setEventItem(itemTempRemove, tableQueue, tableTempRemove);
            menuQueue.getItems().addAll(itemGet, itemRemove, itemTempRemove);
            //
//меню для таблицы временный таблиыц
            ContextMenu menuTempRemove = new ContextMenu();
            MenuItem itemTempQueue = new MenuItem("Кезекке қайтару");
            itemTempQueue.setId("toQueueTempItem");
            setEventItem(itemTempQueue, tableTempRemove, tableQueue);
            menuTempRemove.getItems().addAll(itemTempQueue);

            switch (id) {
                case 1:
                    switch (table.getId()) {
                        case "tableApplication":
                            table.setContextMenu(menuApplication);
                            break;
                    }

                    break;
                case 2:
                    switch (table.getId()) {
                        case "tableQueue":
                            table.setContextMenu(menuQueue);
                            break;
                        case "tableTempRemove":
                            table.setContextMenu(menuTempRemove);
                    }
                    break;
            }

        }
    }
    public void addPlot(Plot plot) {
        // listApplication=getObservableList(Id.getVillageId(),1);

        //  tableApplication.setItems(listApplication);
        listApplication.add(plot);
        tableApplication.refresh();
    }

    private void setEventItem(MenuItem item, TableView from, TableView to) {
        MenuItemListener listener = new MenuItemListener();
        item.setOnAction(event -> {
            Plot plot = null;
            if (item.getId() == "toGetItem") {
                from.getSelectionModel().clearSelection();
                from.getSelectionModel().selectFirst();
                plot = (Plot) from.getSelectionModel().getSelectedItem();
                listener.action(item.getId(), plot, listAnchorPane, from, to);
            }
            if (item.getId() == "toQueueItem") {

                List<Plot> selectedList = from.getSelectionModel().getSelectedItems();
                listener.addQueueMulti(item.getId(), selectedList, listAnchorPane, from, to);
                to.getItems().addAll(selectedList);
                from.getItems().removeAll(selectedList);

            } else {
                plot = (Plot) from.getSelectionModel().getSelectedItem();
                listener.action(item.getId(), plot, listAnchorPane, from, to);
            }


        });
    }



    private void setValueFactory(TableView table) {
        TableColumn<Plot, Integer> columnId = new TableColumn<>("id");
        TableColumn<Plot, String> columnFullname = new TableColumn<>("Аты-жөні");
        TableColumn<Plot, String> columnIin = new TableColumn<>("ЖСН");
        TableColumn<Plot, String> columnApplyDate = new TableColumn<>("Тапсырған күні");
        TableColumn<Plot, String> columnCompletion = new TableColumn<>("Шешім");
        TableColumn<Plot, String> columnVillage = new TableColumn<>("Ауылдық округ");
        TableColumn<Plot, Integer> columnIdVillage = new TableColumn<>("ID округ");
        columnId.setSortable(false);
        columnId.setSortable(false);
        columnFullname.setSortable(false);
        columnIin.setSortable(false);
        columnApplyDate.setSortable(false);
        columnCompletion.setSortable(false);
        columnVillage.setSortable(false);
        columnIdVillage.setSortable(false);
        columnId.setCellValueFactory(new PropertyValueFactory<Plot, Integer>("id"));
        columnFullname.setCellValueFactory(new PropertyValueFactory<Plot, String>("fullName"));
        columnIin.setCellValueFactory(new PropertyValueFactory<Plot, String>("iin"));
        columnCompletion.setCellValueFactory(new PropertyValueFactory<Plot, String>("completion"));
        columnApplyDate.setCellValueFactory(new PropertyValueFactory<Plot, String>("apply_date"));
        columnVillage.setCellValueFactory(new PropertyValueFactory<Plot, String>("village"));
        columnIdVillage.setCellValueFactory(new PropertyValueFactory<Plot, Integer>("id_village"));
        table.getColumns().addAll(columnId, columnFullname, columnIin, columnApplyDate, columnCompletion, columnVillage, columnIdVillage);
    }

    private ObservableList getObservableList(int village, int status) {
        ObservableList<Plot> list = FXCollections.observableArrayList();
        JSONArray array = jsonService.getPlots(village + "/" + status);

        if (array == null) {
            snackbar.show("Бос", 3000);
        }
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = (JSONObject) array.get(i);
                list.add(new Plot(object.getInt("id"), object.getInt("queue"), object.getString("iin"), object.getString("fullName"), object.getString("apply_date"), object.getString("completion"), object.getString("village"), object.getInt("id_village")));

            }
        } catch (NullPointerException e) {
            JFXSnackbar snackbar = new JFXSnackbar(listAnchorPane);
            snackbar.show("Сервермен байланыс жоқ", 3000);
        }
        return list;
    }


    public void refresh(ActionEvent actionEvent) {
        Button source = (Button) actionEvent.getSource();
        ObservableList<Plot> refreshList = null;
        TableView refreshTableView = null;
        switch (source.getId()) {
            case "first":
                refreshList = getObservableList(Id.getVillageId(), 1);
                refreshTableView = tableApplication;
                labelCountApplication.setText(refreshList.size() + "");
                break;
            case "second":
                refreshList = getObservableList(Id.getVillageId(), 2);
                refreshTableView = tableQueue;
                labelCountQueue.setText(refreshList.size() + "");
                break;
            case "third":
                refreshList = getObservableList(Id.getVillageId(), 3);
                refreshTableView = tableGet;
                labelCountGet.setText(refreshList.size() + "");
                break;
            case "fouth":
                refreshList = getObservableList(Id.getVillageId(), 4);
                refreshTableView = tableRefuze;
                labelCountRefuze.setText(refreshList.size() + "");
                break;
            case "five":
                refreshList = getObservableList(Id.getVillageId(), 5);
                refreshTableView = tableRemove;
                labelCountRemove.setText(refreshList.size() + "");
                break;
            case "six":
                refreshList = getObservableList(Id.getVillageId(), 6);
                refreshTableView = tableTempRemove;
                labelCountTempRemove.setText(refreshList.size() + "");
                break;

        }


        refreshTableView.setItems(refreshList);


    }
}
