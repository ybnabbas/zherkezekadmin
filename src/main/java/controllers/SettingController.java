package controllers;


import com.jfoenix.controls.JFXSnackbar;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.paint.Color;
import org.json.JSONObject;
import utils.Prop;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class SettingController {
    public PasswordField oldPasswordField;
    public PasswordField repeatNewPasswordField;
    public PasswordField newPasswordField;
    public Label resultLabel;
    public AnchorPane settingAnchorPane;
    String oldPassword,newPassword,repeatNewPassword;
    Color redColor=Color.RED;
    Color blueColor=Color.BLUE;
    Border redBorder = new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, null, null));
    Border blueBorder = new Border(new BorderStroke(Color.BLUE, BorderStrokeStyle.SOLID, null, null));

    public void change(ActionEvent actionEvent) {
         oldPassword=oldPasswordField.getText();
         newPassword=newPasswordField.getText();
         repeatNewPassword=repeatNewPasswordField.getText();

         if (oldPassword.isEmpty()){
             showResult(oldPasswordField,1,"Ескі құпия сөзді толтырыңыз");

             //
         }else if (newPassword.isEmpty() ||newPassword.length()<6){
             showResult(newPasswordField,1,"Жаңа құпия сөз 6 символдан көп болуы тиіс.");
             //
         }else if (repeatNewPassword.isEmpty()){
             showResult(repeatNewPasswordField,1,"Жаңа құпия сөзді қайталаңыз.");
             //
         }else if (!newPassword.equals(repeatNewPassword)){
             showResult(repeatNewPasswordField,1,"Жаңа құпия сөзді дұрыс қайталаңыз.");
             //
         }else{
             Response response = null;
             Client client = ClientBuilder.newClient();
             JSONObject object =new JSONObject();
             object.put("oldPassword",oldPassword);
             object.put("login",Id.getlogin());
             object.put("newPassword",newPassword);
             try {
                 response = client.target(Prop.getProperty("moderator")+"changepassword").request().post(Entity.entity(object.toString(), MediaType.APPLICATION_JSON_TYPE));

             } catch (Exception e) {
                 e.printStackTrace();
             }
             if (response.getStatus()==200){
                 showResult(newPasswordField,2,"Құпия сөз ауысты!");
             }else if (response.getStatus()==409){
                 showResult(oldPasswordField,1,"Ескі құпия сөз дұрыс емес");
             }else {
                 showResult(oldPasswordField,1,"Белгісіз қате. Администраторға хабарласыңыз");
             }

             //
         }
    }

    private void showResult(TextField field, int status, String text){
        JFXSnackbar snackbar = new JFXSnackbar(settingAnchorPane);
        switch (status){
            case 1:
                field.setBorder(redBorder);
                break;
            case 2:
                oldPasswordField.clear();
                newPasswordField.clear();
                repeatNewPasswordField.clear();
                oldPasswordField.setStyle(" -fx-text-box-border: transparent;");
                newPasswordField.setStyle(" -fx-text-box-border: transparent;");
                repeatNewPasswordField.setStyle(" -fx-text-box-border: transparent;");
                break;
        }
      snackbar.show(text,3000);
    }

    public void cancel(ActionEvent actionEvent) {
        oldPasswordField.clear();
        repeatNewPasswordField.clear();
        newPasswordField.clear();
    }
}
