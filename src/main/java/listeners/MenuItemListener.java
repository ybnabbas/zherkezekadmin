package listeners;

import com.jfoenix.controls.JFXSnackbar;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import org.json.JSONArray;
import org.json.JSONObject;
import pojo.Plot;
import utils.Prop;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


public class MenuItemListener {
    public void addQueueMulti(String itemId, List<Plot> list, AnchorPane anchorPane, TableView from, TableView to){
switch (itemId){
    case "toQueueItem":
        TextInputDialog queueDialog = new TextInputDialog ();
        queueDialog.setTitle("Кезекке қосу");
        queueDialog.setHeaderText(null);
        queueDialog.setContentText("Корытынды нөмірі және күні");
        Optional<String> resultQueueMulti = queueDialog.showAndWait();
        if (resultQueueMulti.isPresent()){
            Client client= ClientBuilder.newClient();
            JSONArray jsonArray = new JSONArray();
            for (Plot plot:list){
                JSONObject object = new JSONObject();
                object.put("id",plot.getId());
                object.put("status",2);
                object.put("id_village",plot.getId_village());
                object.put("completion",resultQueueMulti.get());
                jsonArray.put(object);

            }

            Response response=null;
            try {
                response=client.target(Prop.getProperty("server")+"moderator/addtoqueue").request().post(Entity.entity(jsonArray.toString(), MediaType.APPLICATION_JSON_TYPE));

            }catch (Exception e){
                e.printStackTrace();
            }
            JFXSnackbar snackbar = new JFXSnackbar(anchorPane);
            String queueResultMulti=null;
            System.out.println(jsonArray.toString());
            if (response.getStatus()==200){



                queueResultMulti="Кезекке қосылды";

            }else {
                queueResultMulti="Кезекке қосылмады.Қайталаңыз";
            }snackbar.show(queueResultMulti,3000);
        }
    break;
}
    }




    /**
     *
     * @param itemId
     * @param plot
     * @param anchorPane
     * @param from
     * @param to
     */
    public void action(String itemId, Plot plot, AnchorPane anchorPane, TableView from,TableView to){
        switch (itemId){

            case "toRefuzeItem":
                TextInputDialog refuzeDialog = new TextInputDialog ();
                refuzeDialog.setTitle("Қабылдамау");
                refuzeDialog.setHeaderText(plot.getFullName());
                refuzeDialog.setContentText("Себебін жазыңыз");
                Optional<String> result = refuzeDialog.showAndWait();

                if (result.isPresent()){
                    Client client= ClientBuilder.newClient();

                    JSONObject object =new JSONObject();
                    object.put("id",plot.getId());
                    object.put("status",4);
                    object.put("id_village",plot.getId_village());
                    object.put("other",result.get());
                    Response response=null;
                    try {
                        response=client.target(Prop.getProperty("server")+"moderator/updateuser").request().post(Entity.entity(object.toString(), MediaType.APPLICATION_JSON_TYPE));

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    JFXSnackbar snackbar = new JFXSnackbar(anchorPane);
                    String queueResult=null;
                    if (response.getStatus()==200){
                        from.getItems().remove(plot);
                        to.getItems().add(plot);
                        queueResult="Сәтті аяқталды. Хабарлама жіберілді";

                    }else {
                        queueResult="Қайталаңыз";
                    }snackbar.show(queueResult,3000);

                }
                break;
            case "toGetItem":
                TextInputDialog getDialog = new TextInputDialog ();
                getDialog.setTitle("Жер беру");
                getDialog.setHeaderText(null);
                getDialog.setContentText(plot.getFullName() + ". Шешім нөмірі және күні ");
                Optional<String> resultButton = getDialog.showAndWait();

                if (resultButton.isPresent()){
                    Client client= ClientBuilder.newClient();

                    JSONObject object =new JSONObject();
                    object.put("id",plot.getId());
                    object.put("status",3);
                    object.put("id_village",plot.getId_village());
                    object.put("decision",resultButton.get());
                    Response response=null;
                    try {
                        response=client.target(Prop.getProperty("server")+"moderator/updateuser").request().post(Entity.entity(object.toString(), MediaType.APPLICATION_JSON_TYPE));

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    JFXSnackbar snackbar = new JFXSnackbar(anchorPane);
                    String queueResult=null;
                    if (response.getStatus()==200){
                        from.getItems().remove(plot);
                        to.getItems().add(plot);
                        queueResult="Сәтті аяқталды. Хабарлама жіберілді";

                    }else {
                        queueResult="Қайталаңыз";
                    }snackbar.show(queueResult,3000);

                }
                else {

                } break;
            case "toRemoveItem":
                TextInputDialog removeDialog = new TextInputDialog();
                removeDialog.setTitle("Кезектен шығару");
                removeDialog.setHeaderText(null);
                removeDialog.setContentText(plot.getFullName() +". Шешім нөмірі және күні");
                Optional<String> resultRD =removeDialog.showAndWait();
                JFXSnackbar snackbar = new JFXSnackbar(anchorPane);
                if (resultRD.isPresent()){
                    Client client=ClientBuilder.newClient();
                    JSONObject object = new JSONObject();
                    object.put("id",plot.getId());
                    object.put("status",5);
                    object.put("id_village",plot.getId_village());
                    object.put("decision",resultRD.get());
                    Response response=client.target(Prop.getProperty("server")+"moderator/updateuser").request().post(Entity.entity(object.toString(), MediaType.APPLICATION_JSON_TYPE));
                    String removeResult=null;
                    if (response.getStatus()==200){
                        from.getItems().remove(plot);
                        to.getItems().add(plot);
                        removeResult=plot.getFullName() +" кезектен шығарылды";
                    }else {
                        removeResult="Қайталаңыз";
                    }snackbar.show(removeResult,3000);
                }
                break;

            case "toTempRemoveItem":
                TextInputDialog removeTempDialog = new TextInputDialog();
                removeTempDialog.setTitle("Кезектен уақытша шығару");
                removeTempDialog.setHeaderText(null);
                removeTempDialog.setContentText(plot.getFullName() +". Шешім нөмірі және күні");
                Optional<String> resultTempRD =removeTempDialog.showAndWait();
                JFXSnackbar snackbarTemp = new JFXSnackbar(anchorPane);
                if (resultTempRD.isPresent()){
                    Client client=ClientBuilder.newClient();
                    JSONObject object = new JSONObject();
                    object.put("id",plot.getId());
                    object.put("status",6);
                    object.put("id_village",plot.getId_village());
                    object.put("decision",resultTempRD.get());
                    Response response=client.target(Prop.getProperty("server")+"moderator/updateuser").request().post(Entity.entity(object.toString(), MediaType.APPLICATION_JSON_TYPE));
                    String removeTempResult=null;
                    if (response.getStatus()==200){
                        from.getItems().remove(plot);
                        to.getItems().add(plot);
                        removeTempResult=plot.getFullName() +" кезектен уақытша шығарылды";
                    }else {
                        removeTempResult="Қайталаңыз";
                    }snackbarTemp.show(removeTempResult,3000);
                }
                break;
            case "toQueueTempItem":

                TextInputDialog tempQueueDialog = new TextInputDialog();
                tempQueueDialog.setTitle("Кезеке қайтару");
                tempQueueDialog.setHeaderText(null);
                tempQueueDialog.setContentText(plot.getFullName() +". Шешім нөмірі және күні");
                Optional<String> resultTempQueue=tempQueueDialog.showAndWait();
                JFXSnackbar snackbarTempQueue = new JFXSnackbar(anchorPane);
                if (resultTempQueue.isPresent()){
                    Client client = ClientBuilder.newClient();
                    JSONObject object = new JSONObject();
                    object.put("id",plot.getId());
                    object.put("status",2);
                    object.put("id_village",plot.getId_village());
                    object.put("completion",resultTempQueue.get());
                    Response response=client.target(Prop.getProperty("server")+"moderator/updateuser").request().post(Entity.entity(object.toString(),MediaType.APPLICATION_JSON_TYPE));
                    String tempQueueResult=null;
                    if (response.getStatus()==200){
                        from.getItems().remove(plot);
                        to.getItems().add(plot);
                        tempQueueResult=plot.getFullName() +" кезекке қайтарылды";
                    }else {
                        tempQueueResult="Қайталаңыз";
                    }snackbarTempQueue.show(tempQueueResult, 3000);
                }
                break;
        }
    }

}
