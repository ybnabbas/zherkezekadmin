package listeners;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import pojo.Plot;


public class FilterListener implements ChangeListener<String> {
    ObservableList list;
    TableView table;
    Label count;

    public FilterListener(ObservableList list, TableView table, Label count) {
        this.list = list;
        this.table = table;
        this.count = count;

    }

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

        System.out.println(observable.getValue());
        FilteredList<Plot> filter = new FilteredList<Plot>(list, plot -> true);
// фильтр для textfield
        filter.setPredicate(plot -> {
            if (newValue.isEmpty() && newValue == null) {
                return true;
            }
            String lowercase = newValue.toLowerCase();
             if (plot.getVillage().contains(newValue)) {
                return true;
            } else if(plot.getFullName().toLowerCase().contains(lowercase)) {
                return true;
            }else  if (plot.getIin().toLowerCase().contains(lowercase)){
                 return true;

             }else{
                return false;
             }
        });
        //

        //фильтр для comboBox

        SortedList<Plot> sortedData = new SortedList<>(filter);
        sortedData.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortedData);
        count.setText(table.getItems().size() + "");
    }

}
